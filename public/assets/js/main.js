$(document).ready(function() {
$(".posters img").addClass('clipped')
var $btns = $('.btn').click(function() {
  if (this.id == 'all') {
    $('#projects > div').fadeIn(450);
    $(".posters img").addClass('clipped')

  } else {
    var $el = $('.' + this.id).fadeIn(450);
    $('#projects > div').not($el).hide();
    console.log($el.children().children())
    // $el.children().children().css({"-webkit-clip-path": "inset(0 0 0 0)", 'clip-path': 'inset(0 0 0 0)'});
    $el.children().children().removeClass('clipped')
  }
  $btns.removeClass('active');
  $(this).addClass('active');
})

var paramsCamera = {
    container: document.getElementById('camera'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './assets/img/mobile/camera.json',
};

var paramsGallery = {
    container: document.getElementById('camera'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './assets/img/mobile/gallery.json',
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid meet'
    }

};


var paramsText = {
    container: document.getElementById('camera'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: './assets/img/mobile/text.json',
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid meet'
    }

};



 lottie.loadAnimation(paramsCamera);
 lottie.loadAnimation(paramsGallery);
 lottie.loadAnimation(paramsText);


$(".zoom").click(function(){
	 $("#projects").blur()
	 $(this).css("transform","scale(1.5)")
	// console.log($("#projects").children().children().children())
	// $("#projects").css({
 //        'filter': 'grayscale(0.5) blur(5px)',
 //        'opacity': '0.8'
 //    })

    // $(".overlay").css({'display':"block",
    //     'filter': 'grayscale(0.5) blur(5px)',
    //     'opacity': '0.8'
    // })


$(this).focus()
})
})



